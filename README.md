# This readme was actually updated

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

### `npm start`

Runs the app in the development mode.\
Open [http://127.0.0.1:3000/query-dashboard-ui](http://127.0.0.1:3000/query-dashboard-ui) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.

### `npm run cypress-with-ui`

Ensure the app is already running via `npm start`
This will launch the Cypress test runner UI where you can select the tests to run as well as watch the tests run in browser.
### `npm run cypress-headless`

Ensure the app is already running via `npm start`
This will run the cypress tests without UI and you will see output of tests and results in the terminal

### `npm run build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `npm run eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.

# General Dev Notes
Keywords - React, TypeScript, MUI5 (Material UI version 5.*), MUI5 'sx' prop, css modules

##Styling
The base (global-like) styling is the styling built into the MUI react components. If you visit the docs, you can look through the different components/options to get an idea. It's worth noting that although the MUI components have base styling, it is not the same as a root/global css file or theme. The base 'index.css' file is the only css file that will attempt to apply it's rules EVERYWHERE it can on the page.

Any styling tools/implementations done 'these days' has the ability to effect ONLY the desired component. As you look through the component folders, you will see a 1to1 parity of 'ComponentName.tsx' to 'ComponentName.module.css'. In the tsx file, you will find a reference to the module.css file it pairs with. Any styling written in a module.css file will ONLY attempt to apply it's rules to component that tries to use it (referred to as locally scoped css), and even more so, to the specific element that tries to reference it via styles.makethisComponentsColorRed for example. That should hopefully be enough to get you started.

The other locally scoped implementation utilized in this app is the 'sx' property attached to most MUI5 components. It is a MUI5 way to alter styling and is pretty powerful as well.

There should be enouogh examples already in place to get an idea of how to remove/edit/update styling moving forward. AND one of the best parts of both of these implementations is that it is much easier to be confident that altering styling will not harm similiar items elsewhere in the application.