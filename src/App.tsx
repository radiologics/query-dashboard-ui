import React, { useEffect, useState } from 'react'
import components from './components'
import { Container } from '@mui/material'
import {
  getQueries,
  getUserProfile,
  initialQueryRequest,
  QueryRequest,
  QueryResponse,
  UserProfile,
} from './http/services'
import { getSetAuthCookies } from './http/client'
const { Header, QueryListTable, StatisticsCards } = components

function App() {
  const [userProfile, setUserProfile] = useState<UserProfile>()
  const [assignedToMe, setAssignedToMe] = useState<boolean>(true)
  const [queryRequest, setQueryRequest] = useState<QueryRequest>(initialQueryRequest)
  const [queryResponse, setQueryResponse] = useState<QueryResponse>()
  const [fetchingData, setFetchingData] = useState(false)
  const [tabsValue, setTabsValue] = useState(0)

  // only on page load, handle auth and make initial query request
  useEffect(() => {
    getSetAuthCookies().then(() => {
      getQueries(initialQueryRequest).then((response: QueryResponse) => {
        setQueryResponse(response)
      })
      getUserProfile().then((response: UserProfile) => {
        setUserProfile(response)
      })
    })
  }, [])

  // watch for when the queryRequest state object is updated and then fetch fresh query data
  useEffect(() => {
    if (queryRequest !== undefined) {
      setFetchingData(true)
      getQueries(queryRequest).then((response: QueryResponse) => {
        setQueryResponse(response)
        setFetchingData(false)
      })
    }
  }, [queryRequest])

  useEffect(() => {
    if (tabsValue === 1) {
      setAssignedToMe(false)
    }
  }, [tabsValue])

  return (
    <div className='App'>
      <Container maxWidth='lg'>
        {queryResponse?.groupResult && (
          <React.Fragment>
            <Header
              assignedToMe={assignedToMe}
              setAssignedToMe={setAssignedToMe}
              tabsValue={tabsValue}
              setTabsValue={setTabsValue}
              queryRequestGroups={queryRequest.groups}
              setQueryRequest={setQueryRequest}
              queryResponseGroups={queryResponse?.groupResult!}
            />
            <React.Fragment>
              <QueryListTable
                tabsValue={tabsValue}
                username={userProfile?.username || ''}
                assignedToMe={assignedToMe}
                queryResults={queryResponse?.searchResult[0]?.data || []}
                fetchingData={fetchingData}
              />
              <StatisticsCards tabsValue={tabsValue} queryGroupResults={queryResponse?.groupResult!} />
            </React.Fragment>
          </React.Fragment>
        )}
      </Container>
    </div>
  )
}

export default App
