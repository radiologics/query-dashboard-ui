export const handleValueFormatting = (label: any, override?: string, overrideValue?: string) => {
  // handle difference between value and desired display wording
  // handling for multi-group Status dropdown - laebl will be of type StatusOption
  let labelValue = label.value ? label.value : label
  if (label === 'active') {
    labelValue = 'Session Active'
  } else if (label === 'locked') {
    labelValue = 'Session Locked'
  } else if (label === 'createby') {
    labelValue = 'Opened By'
  }
  // ensure the first letter of each word is capitalized
  const titleCase = labelValue
    .toLowerCase()
    .split(' ')
    .map((word: string) => {
      return word.charAt(0).toUpperCase() + word.slice(1)
    })
    .join(' ')

  return titleCase
}