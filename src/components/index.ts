import Header from './Header/Header'
import QueryListTable from './QueryListTable/QueryListTable'
import StatisticsCards from './StatisticsCards/StatisticsCards'
const components = {
  Header,
  QueryListTable,
  StatisticsCards
}

export default components
