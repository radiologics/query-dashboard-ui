import { Card, CardHeader, CardContent, Grid } from '@mui/material'
import styles from './StatisticsCards.module.css'
import { Count, GroupResult } from '../../http/services'
import { CATEGORIES, CREATE_BY, PROJECTS, REVIEW, SESSION_STATUS, STATUS } from '../constants'
import { handleValueFormatting } from '../utils'

interface StatisticsCardsProps {
  tabsValue: Number
  queryGroupResults: GroupResult[]
}

interface StatCard {
  name: string
  counts: Count[]
}

function StatisticsCards({ tabsValue, queryGroupResults }: StatisticsCardsProps) {
  const projects: GroupResult = queryGroupResults.find((item: GroupResult) => {
    return item.name === PROJECTS
  }) as GroupResult

  const openedBy: GroupResult = queryGroupResults.find((item: GroupResult) => {
    return item.name === CREATE_BY
  }) as GroupResult

  const categories: GroupResult = queryGroupResults.find((item: GroupResult) => {
    return item.name === CATEGORIES
  }) as GroupResult

  const statuses: GroupResult[] = queryGroupResults.filter((item: GroupResult) => {
    return item.name === STATUS || item.name === REVIEW
  })

  const sessionStatus: GroupResult = queryGroupResults.find((item: GroupResult) => {
    return item.name === SESSION_STATUS
  }) as GroupResult

  return (
    <Grid sx={{ display: tabsValue === 1 ? 'flex' : 'none' }} container spacing={1}>
      <Grid item xs={3}>
        <StatsCard name={projects.name} counts={projects.counts} />
      </Grid>
      <Grid item xs={3}>
        <StatsCard name={openedBy.name} counts={openedBy.counts} />
      </Grid>
      <Grid item xs={3}>
        <StatsCard name={categories.name} counts={categories.counts} />
      </Grid>
      <Grid item xs={3}>
        <StatsCard name='Status' counts={[...statuses[0].counts, ...statuses[1].counts]} />
        <div className={styles.sessionCard}>
          <StatsCard name='Session' counts={sessionStatus.counts} />
        </div>
      </Grid>
    </Grid>
  )
}
export default StatisticsCards
/**
 * keeping this react component in-file as this is the only purpose of the Statistics tab ATM
 * @param name - card header name
 * @param counts - Array of Count objects to list out
 * @returns
 */
function StatsCard({ name, counts }: StatCard): JSX.Element {
  return (
    <Card sx={{ maxWidth: 350 }} className={styles.cardContainer}>
      <CardHeader title={handleValueFormatting(name!)} className={styles.cardHeader} />
      <CardContent className={styles.cardContent}>
        <ul className={styles.cardList}>
          {counts?.map((option: Count) => {
            return (
              <li key={option.value} className={styles.cardListItem}>
                <span>{handleValueFormatting(option.value)}</span>
                <span className={styles.cardListItemRightSpan}>{option.count}</span>
              </li>
            )
          })}
        </ul>
      </CardContent>
    </Card>
  )
}
