export const PROJECTS = 'projects'
export const CATEGORIES = 'categories'
export const STATUS = 'status'
export const REVIEW = 'review'
export const SESSION_STATUS = 'sessionstatus'
export const CREATE_BY = 'createby'

// primary blue variations - mimicked in root index.css
export const blue_1 = '#2e72f6'
export const blue_2 = '#255BC5'
export const blue_3 = '#D5E3FD'
