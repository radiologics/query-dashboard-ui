import React from 'react'
import { Datum } from '../../http/services'
import { DataGrid, GridColDef, GridToolbarContainer, GridToolbarExport } from '@mui/x-data-grid'
import CheckCircleIcon from '@mui/icons-material/CheckCircle'
import { Grid } from '@mui/material'
import styles from './QueryListTable.module.css'
import { format } from 'date-fns'
import { createPortal } from 'react-dom'
import http from '../../http'
import { blue_1, blue_3 } from '../constants'
interface QueryListTableProps {
  tabsValue: Number
  username: String
  assignedToMe: boolean
  queryResults: Datum[]
  fetchingData: boolean
}

function QueryListTable({ tabsValue, username, assignedToMe, queryResults, fetchingData }: QueryListTableProps) {
  /**
   * two parts -
   * 1 - explicitly define the Toolbar to only show the Export button -
   * 2 - wrap it as a Portal, so the toolbar can functionally work as usual with the DataGrid implementaiton,
   * and allow us to attach it to an element elsewhere on the page (in our case, the Header)
   */
  const CustomToolbar = () => {
    return createPortal(
      <GridToolbarContainer className={styles.exportButtonContainer}>
        <GridToolbarExport disabled={queryResults.length === 0} printOptions={{ disableToolbarButton: true }} />
      </GridToolbarContainer>,
      document.getElementById('portal-export-button') as Element,
    )
  }

  const columns: GridColDef[] = [
    { field: 'project', headerName: 'Project', width: 200 },
    {
      field: 'last_updated',
      headerName: 'Last Update',
      width: 140,
      renderCell: (params) => {
        return (
          <div>
            <div>{params.row['last_updated']}</div>
            <div style={{ color: '#666666' }}>by {params.row['last_updated_by']}</div>
          </div>
        )
      },
    },
    { field: 'title', headerName: 'Title', width: 200 },
    {
      field: 'created',
      headerName: 'Opened',
      width: 140,
      renderCell: (params) => {
        return (
          <div>
            <div>{params.row['created']}</div>
            <div style={{ color: '#666666' }}>by {params.row['created_by']}</div>
          </div>
        )
      },
    },
    {
      field: 'subject_label',
      headerName: 'Subject',
      width: 180,
      renderCell: (params) => {
        const sessionUrl = http.urls.TO_SUBJECT_DATA.replace(':subject_id', params.row['subject_id']).replace(
          ':project',
          params.row['project'],
        )
        return (
          <div>
            <a href={sessionUrl} className={styles.linkCellValue}>
              {params.row['subject_label']}
            </a>
          </div>
        )
      },
    },
    {
      field: 'label',
      headerName: 'Session',
      width: 180,
      renderCell: (params) => {
        const sessionUrl = http.urls.TO_SESSION_DATA.replaceAll(':xsi_type', params.row['xsi_type'])
          .replace(':data_id', params.row['data_id'])
          .replace(':project', params.row['project'])
        return (
          <div>
            <a href={sessionUrl} className={styles.linkCellValue}>
              {params.row['label']}
            </a>
          </div>
        )
      },
    },
    {
      field: 'status',
      headerName: 'Closed',
      headerAlign: 'center',
      align: 'center',
      flex: 1,
      renderCell: (params) => {
        const isClosed = params.row['status'].toLowerCase() === 'closed'
        return <div>{isClosed ? <CheckCircleIcon htmlColor='#00B778' /> : ''}</div>
      },
    },
    // fields hidden (via 'columnVisibilityModel') but needed to nicely access in 'renderCell'
    { field: 'created_by' },
    { field: 'xsi_type' },
    { field: 'data_id' },
    { field: 'subject_id' },
  ]

  let rows = queryResults?.map((row: Datum) => {
    return {
      ...row,
      created: format(new Date(row.created), 'yyyy-MM-dd'),
      last_updated: format(new Date(row.last_updated), 'yyyy-MM-dd'),
    }
  })

  if (assignedToMe === true) {
    rows = rows.filter((row: Datum) => {
      return row.assigned_to === username 
    })
  }

  return (
    <React.Fragment>
      {queryResults && (
        <Grid
          sx={{ display: tabsValue === 0 ? 'flex' : 'none' }}
          container
          spacing={1}
          className={styles.tableContainer}
        >
          <Grid item xs={12}>
            <DataGrid
              rows={rows}
              columns={columns}
              disableSelectionOnClick={true}
              disableColumnMenu={true}
              columnBuffer={columns.length}
              headerHeight={48}
              loading={fetchingData}
              initialState={{
                sorting: {
                  sortModel: [{ field: 'project', sort: 'asc' }],
                },
                columns: {
                  columnVisibilityModel: {
                    created_by: false,
                    xsi_type: false,
                    data_id: false,
                    subject_id: false,
                  },
                },
              }}
              components={{
                Toolbar: CustomToolbar,
              }}
              componentsProps={{
                toolbar: {
                  sx: {
                    '& .MuiDataGrid-toolbarContainer': {
                      paddingTop: '0px',
                    },
                  },
                },
                baseButton: {
                  variant: 'outlined',
                  sx: {
                    fontSize: '14px',
                    height: '40px',
                    textTransform: 'none',
                    color: 'white',
                    backgroundColor: blue_1,
                    borderColor: blue_3,
                    '&:hover:not(:disabled)': {
                      color: blue_1,
                      backgroundColor: 'white',
                    },
                  },
                },
              }}
              sx={{
                '& .MuiDataGrid-columnHeaders': {
                  backgroundColor: '#F7F8F9',
                },
                '& .MuiDataGrid-footerContainer': {
                  backgroundColor: '#F7F8F9',
                },
                '& .MuiDataGrid-columnHeaderTitle': {
                  fontSize: '16px',
                },
                '& .MuiDataGrid-renderingZone': {
                  maxHeight: 'none !important',
                },
                '& .MuiDataGrid-cell': {
                  lineHeight: 'unset !important',
                  maxHeight: 'none !important',
                  whiteSpace: 'normal',
                },
                '& .MuiDataGrid-row': {
                  maxHeight: 'none !important',
                },
                '& .MuiDataGrid-iconSeparator': {
                  display: 'none',
                },
              }}
            />
          </Grid>
        </Grid>
      )}
    </React.Fragment>
  )
}

export default QueryListTable
