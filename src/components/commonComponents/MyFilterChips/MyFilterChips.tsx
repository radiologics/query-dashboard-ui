import { Chip } from '@mui/material'
import cloneDeep from 'lodash.clonedeep'
import React, { useEffect, useState } from 'react'
import { Group, QueryRequest } from '../../../http/services'
import { blue_1 } from '../../constants'
import { handleValueFormatting } from '../../utils'
import styles from './MyFilterChips.module.css'

interface ChipItem {
  name: string
  selected: string[]
}

interface MyFilterChipsProps {
  setQueryRequest: React.Dispatch<React.SetStateAction<QueryRequest>>
  queryRequestGroups: Group[]
}

function MyFilterChips({ setQueryRequest, queryRequestGroups }: MyFilterChipsProps) {
  const [myChips, setChips] = useState<ChipItem[]>([])

  useEffect(() => {
    let chips: ChipItem[] = []
    queryRequestGroups.forEach((group: Group) => {
      chips.push({ name: group.name, selected: group.selected })
    })
    setChips(chips)
  }, [queryRequestGroups])

  const handleFilterChipDeletion = (filterName: string, value: string) => {
    setQueryRequest((currentQuery: QueryRequest) => {
      let query = cloneDeep(currentQuery)
      const relevantGroup = query.groups.find((group: Group) => group.name === filterName)
      relevantGroup?.selected.splice(relevantGroup?.selected.indexOf(value), 1)
      return query
    })
  }

  return (
    <React.Fragment>
      {myChips?.map((filter: ChipItem) => {
        return filter.selected.map((item: string) => {
          return (
            <Chip
              key={item}
              label={handleValueFormatting(item)}
              onDelete={() => handleFilterChipDeletion(filter.name, item)}
              className={styles.filterChip}
              sx={{
                '& .MuiChip-deleteIconOutlinedColorDefault': {
                  color: blue_1,
                  opacity: 0.5,
                },
              }}
            />
          )
        })
      })}
    </React.Fragment>
  )
}

export default MyFilterChips
