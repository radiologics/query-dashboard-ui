import { Autocomplete, Checkbox, TextField, Popper } from '@mui/material'
import CheckBoxOutlineBlankIcon from '@mui/icons-material/CheckBoxOutlineBlank'
import CheckBoxIcon from '@mui/icons-material/CheckBox'
import { blue_3 } from '../../constants'
import styles from './MyAutoComplete.module.css'

interface MyAutoCompleteProps {
  id: string
  values: any[]
  viewableOptions: any[]
  handleOptionEqualToValue: (option: any, value: any) => boolean
  handleOnChange: Function
  optionLabel: (option: any) => string
  isChecked: (option: any) => boolean
  labelText: string
  textFieldWidth: string
}

function MyAutoComplete({
  id,
  values,
  viewableOptions,
  handleOptionEqualToValue,
  handleOnChange,
  optionLabel,
  isChecked,
  labelText,
  textFieldWidth,
}: MyAutoCompleteProps) {
  const icon = <CheckBoxOutlineBlankIcon fontSize='small' />
  const checkedIcon = <CheckBoxIcon fontSize='small' />
  const textFieldInputStying = {
    '& .MuiOutlinedInput-notchedOutline': {
      borderColor: blue_3,
    },
  }

  return (
    <Autocomplete
      multiple
      id={id}
      size='small'
      value={values}
      options={viewableOptions}
      isOptionEqualToValue={(option, value) => handleOptionEqualToValue(option, value)}
      onChange={(event, value) => handleOnChange(value)}
      disableCloseOnSelect
      getOptionLabel={(option) => optionLabel(option)}
      renderTags={() => null}
      renderOption={(props, option) => (
        <li {...props} key={option.value ? option.value : option} className={styles.listOption}>
          <Checkbox
            icon={icon}
            checkedIcon={checkedIcon}
            className={styles.dropdownCheckbox}
            checked={isChecked(option)}
          />
          <span className={styles.dropdownListValue}>{option.value ? option.value : option}</span>
        </li>
      )}
      renderInput={(params) => (
        <TextField
          {...params}
          label={labelText}
          style={{ width: textFieldWidth }}
          sx={textFieldInputStying}
          InputLabelProps={{ className: styles.textFieldLabel }}
        />
      )}
      PopperComponent={(props) => (
        <Popper {...props} className={styles.dropdownListContainer} placement='bottom-start' />
      )}
    />
  )
}

export default MyAutoComplete
