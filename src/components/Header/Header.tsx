import { Grid, Tab, Tabs, Checkbox, FormGroup, FormControlLabel, Tooltip, Zoom } from '@mui/material'
import cloneDeep from 'lodash.clonedeep'
import React from 'react'
import { Count, Group, GroupResult, QueryRequest } from '../../http/services/queries'
import styles from './Header.module.css'
import { blue_1, CATEGORIES, CREATE_BY, PROJECTS, REVIEW, SESSION_STATUS, STATUS } from '../constants'
import { MyAutoComplete, MyFilterChips } from '../commonComponents'
interface HeaderProps {
  assignedToMe: boolean
  setAssignedToMe: React.Dispatch<React.SetStateAction<boolean>>
  tabsValue: Number
  setTabsValue: React.Dispatch<React.SetStateAction<number>>
  queryRequestGroups: Group[]
  setQueryRequest: React.Dispatch<React.SetStateAction<QueryRequest>>
  queryResponseGroups: GroupResult[]
}

interface StatusOption {
  value: string
  groupName: string
}

function Header({
  assignedToMe,
  setAssignedToMe,
  tabsValue,
  setTabsValue,
  queryRequestGroups,
  setQueryRequest,
  queryResponseGroups,
}: HeaderProps) {
  const handleTabsChange = (event: React.SyntheticEvent, newValue: number) => {
    setTabsValue(newValue)
  }

  const projectGroupResult = queryResponseGroups?.find((item: GroupResult) => {
    return item.name === PROJECTS
  }) as GroupResult

  const categoriesGroupResult = queryResponseGroups?.find((item: GroupResult) => {
    return item.name === CATEGORIES
  }) as GroupResult

  const statusesGroupResults = queryResponseGroups?.filter((item: GroupResult) => {
    return item.name === STATUS || item.name === REVIEW || item.name === SESSION_STATUS
  }) as GroupResult[]

  const openedByGroupResult = queryResponseGroups?.find((item: GroupResult) => {
    return item.name === CREATE_BY
  }) as GroupResult

  /**
   * Handles Header Dropdowns - Projects, Categories, Opened By
   * @param groupName reference Group interface - the different filter group lists
   * @param allSelectedValuesForTheGroup Autocomplete component passes all 'checked' values, not just the recently selected one
   */
  const updateQueryRequestAndFetch = (groupName: string, allSelectedValuesForTheGroup: string[]) => {
    setQueryRequest((currentQuery: QueryRequest) => {
      let query: QueryRequest = cloneDeep(currentQuery)
      for (const group of query.groups) {
        if (group.name === groupName) {
          group.selected = allSelectedValuesForTheGroup
        }
      }
      return query
    })
  }

  /**
   * the Status dropdown is a cumulative list of Group lists - status, review, and sessionstatus.
   * This requires bringing the options together via 'statusOptions', and then massaging here
   * to fit back to the API RequestObject shape
   * @param allSelectedOptions reference StatusOption interface
   */
  const handleStatusSelectionChange = (allSelectedOptions: StatusOption[]) => {
    setQueryRequest((currentQuery: QueryRequest) => {
      let query: QueryRequest = cloneDeep(currentQuery)

      // since Autocomplete only passes 'checked' values, empty status filters to then add to.
      query.groups.forEach((group: Group) => {
        if (group.name === STATUS || group.name === REVIEW || group.name === SESSION_STATUS) {
          group.selected = []
        }
      })

      for (const group of query.groups) {
        allSelectedOptions.forEach((option: StatusOption) => {
          if (group.name === option.groupName) {
            if (!group.selected.includes(option.value)) {
              group.selected.push(option.value)
            }
          }
        })
      }
      return query
    })
  }

  // flatten the statusGroupResults to be simple for both the Autocomplete to work with,
  // and to make it easier to map to it's original parent category list when updating the queryRequest
  let statusOptions: StatusOption[] = []
  statusesGroupResults?.forEach((group: GroupResult) => {
    group?.counts.forEach((option: Count) => {
      statusOptions.push({ value: option.value, groupName: group.name })
    })
  })

  const statusesDropdownSelectedValues: StatusOption[] = []
  queryRequestGroups
    .filter((group: Group) => group.name === STATUS || group.name === REVIEW || group.name === SESSION_STATUS)
    .forEach((group: Group) => {
      group.selected.forEach((option: string) => {
        statusesDropdownSelectedValues.push({ groupName: group.name, value: option } as StatusOption)
      })
    })

  return (
    queryResponseGroups && (
      <React.Fragment>
        <Grid container spacing={1} className={styles.titleGrid}>
          <Grid item>
            <h3 className={styles.inlineh3}>Query Dashboard</h3>
          </Grid>
        </Grid>

        <Grid container spacing={1} className={styles.tabsDropdownsContainer}>
          <Grid item className={styles.tabsGroup}>
            <Tabs value={tabsValue} onChange={handleTabsChange} aria-label='query list tab and statistics tab'>
              <Tab label={'Query List'} className={styles.tab} />
              <Tab label={'Statistics'} className={styles.tab} />
            </Tabs>
          </Grid>

          <Grid item className={styles.dropdownGroup}>
            {projectGroupResult?.counts && (
              <MyAutoComplete
                id='projects-dropdown'
                values={queryRequestGroups.find((group: Group) => group.name === PROJECTS)?.selected!}
                viewableOptions={projectGroupResult?.counts.map((option: Count) => option.value)!}
                handleOptionEqualToValue={(option, value) => option === value}
                handleOnChange={(value: string[]) => updateQueryRequestAndFetch(PROJECTS, value)}
                optionLabel={(option: string) => option}
                isChecked={(option: string) =>
                  queryRequestGroups.find((group: Group) => group.name === PROJECTS)?.selected.includes(option)!
                }
                labelText='Projects'
                textFieldWidth='125px'
              />
            )}

            {categoriesGroupResult?.counts && (
              <MyAutoComplete
                id='categories-dropdown'
                values={queryRequestGroups.find((group: Group) => group.name === CATEGORIES)?.selected!}
                viewableOptions={categoriesGroupResult?.counts.map((option: Count) => option.value)!}
                handleOptionEqualToValue={(option, value) => option === value}
                handleOnChange={(value: string[]) => updateQueryRequestAndFetch(CATEGORIES, value)}
                optionLabel={(option: string) => option}
                isChecked={(option: string) =>
                  queryRequestGroups.find((group: Group) => group.name === CATEGORIES)?.selected.includes(option)!
                }
                labelText='Categories'
                textFieldWidth='140px'
              />
            )}

            {statusOptions && (
              <MyAutoComplete
                id='status-dropdown'
                values={statusesDropdownSelectedValues}
                viewableOptions={statusOptions}
                handleOptionEqualToValue={(option: StatusOption, value: StatusOption) => option.value === value.value}
                handleOnChange={(value: StatusOption[]) => handleStatusSelectionChange(value)}
                optionLabel={(option: StatusOption) => option.value}
                isChecked={(option: StatusOption) =>
                  statusesDropdownSelectedValues.some(
                    (statusOption: StatusOption) => statusOption.value === option.value,
                  )
                }
                labelText='Status'
                textFieldWidth='110px'
              />
            )}

            {openedByGroupResult?.counts && (
              <MyAutoComplete
                id='openedby-dropdown'
                values={queryRequestGroups.find((group: Group) => group.name === CREATE_BY)?.selected!}
                viewableOptions={openedByGroupResult?.counts.map((option: Count) => option.value)!}
                handleOptionEqualToValue={(option, value) => option === value}
                handleOnChange={(value: string[]) => updateQueryRequestAndFetch(CREATE_BY, value)}
                optionLabel={(option: string) => option}
                isChecked={(option: string) =>
                  queryRequestGroups.find((group: Group) => group.name === CREATE_BY)?.selected.includes(option)!
                }
                labelText='Opened By'
                textFieldWidth='140px'
              />
            )}

            {openedByGroupResult?.counts && (
              <Tooltip
                title='Statistics counts are for all assigned users'
                arrow={true}
                TransitionComponent={Zoom}
                componentsProps={{
                  tooltip: {
                    sx: { fontSize: 14 },
                  },
                }}
                disableHoverListener={tabsValue === 0}
              >
                <FormGroup>
                  <FormControlLabel
                    control={
                      <Checkbox
                        size='small'
                        checked={assignedToMe}
                        disabled={tabsValue === 1}
                        onChange={() => {
                          setAssignedToMe(!assignedToMe)
                        }}
                        sx={{
                          color: blue_1,
                          '&.MuiFormControlLabel-label': {
                            fontSize: 30,
                          },
                          '&.Mui-checked': {
                            color: blue_1,
                          },
                        }}
                      />
                    }
                    label='Assigned to me'
                    classes={{ label: styles.checkboxLabel }}
                    className={styles.checkboxBorder}
                  />
                </FormGroup>
              </Tooltip>
            )}

            <div id='portal-export-button' />
          </Grid>
        </Grid>
        <div className={styles.chipContainer}>
          <MyFilterChips setQueryRequest={setQueryRequest} queryRequestGroups={queryRequestGroups} />
        </div>
      </React.Fragment>
    )
  )
}

export default Header
