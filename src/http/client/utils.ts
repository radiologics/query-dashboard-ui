import { Buffer } from 'buffer'

export const getCookie = (cookieName: string): string => {
  let cookie: { [key: string]: string } = {}
  document.cookie.split(';').forEach(function (el) {
    let [key, value] = el.split('=')
    cookie[key.trim()] = value
  })
  return cookie[cookieName]
}

export type REQUEST_TYPE = {
  GET: 'GET'
  PUT: 'PUT'
  POST: 'POST'
}
export type RESPONSE_TYPE = {
  JSON: 'JSON'
  TEXT: 'TEXT'
}

export interface requestProps {
  redirect: RequestRedirect
  method: string
  withCredentails: boolean
  credentials: RequestCredentials
  headers?: {
    Authorization?: string
    'Content-Type'?: string
  }
  body?: any
}

export const getRequestObject = (
  method: string,
  data?: any,
): requestProps => {
  if (process.env.NODE_ENV === 'development') {
    const username = process.env.REACT_APP_XNAT_USERNAME
    const password = process.env.REACT_APP_XNAT_PASSWORD
    const auth = `Basic ${Buffer.from(`${username}:${password}`).toString(
      'base64',
    )}`
    return {
      redirect: 'follow',
      method: method,
      withCredentails: true,
      credentials: 'include',
      headers: {
        Authorization: auth,
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(data),
    }
  } else {
    return {
      redirect: 'error',
      method: method,
      withCredentails: true,
      credentials: 'include',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(data),
    }
  }
}
