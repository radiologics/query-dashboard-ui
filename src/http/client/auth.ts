import ApiClient from './index'
import { GET_AUTH } from '../urls'
export const getSetAuthCookies = async () => {
  const response = (await ApiClient(GET_AUTH, 'GET', 'TEXT')) as String
  const splitResponse = response.split(';')
  const jsessionValue = splitResponse[0].trim()
  document.cookie = `JSESSIONID=${jsessionValue};path=/`
  const csrf = splitResponse[1].trim()
  document.cookie = `${csrf};path=/`
}
