import configureClient from './configureClient'
import { getSetAuthCookies } from './auth'
import { getCookie } from './utils'
const ApiClient = configureClient
export default ApiClient()
export { getSetAuthCookies, getCookie }
