import {
  getRequestObject,
  requestProps,
  REQUEST_TYPE,
  RESPONSE_TYPE,
} from './utils'
const requestAndResponseHandler = async (
  url: string,
  requetObject: requestProps,
  responseType: keyof RESPONSE_TYPE,
): Promise<JSON | String> => {
  return fetch(url, requetObject)
    .then(async (response: Response) => {
      if (response.status === 200) {
        if (responseType === 'JSON') {
          return await response.json()
        } else if (responseType === 'TEXT') {
          return await response.text()
        }
      }
      return response
    })
    .catch(() => {
      return Promise.reject()
    })
}

export default function configureClient() {
  return (
    url: any,
    methodType: keyof REQUEST_TYPE,
    responseType: keyof RESPONSE_TYPE,
    data?: any,
  ) => {
    const requestObject: requestProps = getRequestObject(methodType, data)
    return requestAndResponseHandler(url, requestObject, responseType)
  }
}
