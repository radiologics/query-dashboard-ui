const domain = process.env.NODE_ENV === 'development' ? process.env.REACT_APP_XNAT_DOMAIN : ''

export const GET_AUTH = `${domain}/data/JSESSION?CSRF=True`

export const GET_USER_PROFILE = `${domain}/xapi/users/profile`

// Retrieve all query related data
export const GET_QUERIES = `${domain}/data/queries/dashboard?XNAT_CSRF=:XNAT_CSRF`

// Query Table - Session column cell value link
export const TO_SESSION_DATA = `${domain}/app/action/DisplayItemAction/search_element/:xsi_type/search_field/:xsi_type.ID/search_value/:data_id/popup/false/project/:project`

// Query Table - Subject column cell value link
export const TO_SUBJECT_DATA = `${domain}/app/action/DisplayItemAction/search_element/xnat:subjectData/search_field/xnat:subjectData.ID/search_value/:subject_id/popup/false/project/:project`
