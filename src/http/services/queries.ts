import ApiClient, { getCookie } from '../client'
import { GET_QUERIES } from '../urls'

export const getQueries = async (
  queryRequest: QueryRequest,
): Promise<QueryResponse> => {
  const api = GET_QUERIES.replaceAll(':XNAT_CSRF', getCookie('XNAT_CSRF'))

  return await ApiClient(api, 'POST', 'JSON', queryRequest).then(
    (response: any) => {
      return response as QueryResponse
    },
  )
}

export interface Group {
  name: string
  selected: string[]
  all: string[]
}

export interface Result {
  name: string
  limit: number
  offset: number
  sort: string
  order: number
}

export interface Request {
  groups: Group[]
  results: Result[]
}

export interface Count {
  value: string
  count: number
}

export interface GroupResult {
  name: string
  counts: Count[]
  keys: string[]
}

export interface Datum {
  subject_id: string
  subject_label: string
  created: any
  description: string
  project: string
  label: string
  last_updated: any
  last_updated_by: string
  title: string
  created_by: string
  enabled: boolean
  can_reupload?: any
  data_id: string
  review: string
  req_data_reupload: boolean
  disabled: number
  id: number
  xsi_type: string
  session_status: string
  timestamp: any
  assigned_to: string
  status: string
}

export interface SearchResult {
  draw: number
  name: string
  startIndex: number
  data: Datum[]
  keys: string[]
  totalRecords: number
}

export interface QueryRequest {
  groups: Group[]
  results: Result[]
}
export interface QueryResponse {
  request: Request
  groupResult: GroupResult[]
  searchResult: SearchResult[]
}

export const initialQueryRequest: QueryRequest = {
  groups: [
    {
      name: 'projects',
      selected: [],
      all: [],
    },
    {
      name: 'status',
      selected: [],
      all: [],
    },
    {
      name: 'review',
      selected: [],
      all: [],
    },
    {
      name: 'categories',
      selected: [],
      all: [],
    },
    {
      name: 'createby',
      selected: [],
      all: [],
    },
    {
      name: 'sessionstatus',
      selected: [],
      all: [],
    },
  ],
  results: [
    {
      name: 'results',
      sort: 'id',
      order: 1,
      offset: 0,
      limit: 100,
    },
  ],
}
