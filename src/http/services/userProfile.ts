import ApiClient from '../client'
import { GET_USER_PROFILE } from '../urls'

export const getUserProfile = async () => {
  return await ApiClient(GET_USER_PROFILE, 'GET', 'JSON').then((response: any) => {
    return response as UserProfile
  })
}

export interface UserProfile {
  lastModified: number
  enabled: boolean
  username: string
  id: number
  email: string
  secured: boolean
  verified: boolean
  lastName: string
  firstName: string
  lastSuccessfulLogin: number
}
