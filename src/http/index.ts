import * as client from './client'
import * as  services from './services'
import * as urls from './urls'
const http = {
  client,
  services,
  urls
}

export default http