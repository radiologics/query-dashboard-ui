declare namespace Cypress {
  interface Chainable {
    /**
     * Cypress does not currently have a good way to handle waiting for re-render, which
     * is a core implementaion of React. Enforcing an ambguous wait time is
     * a poor but simple workaround for now.
     * https://github.com/cypress-io/cypress/issues/7306#issuecomment-1098106831
     * 
     */
    click(): Chainable<JQuery<HTMLElement>>
    /**
     * Selects the first option from each of the 4 dropdowns and checks that 4 filter
     * chips are present afterwords
     */
    makeSelectionFromEachDropdown(): Chainable<Element>
    /**
     * Selects all filter chips and expects there to be the supplied number
     */
    numOfFilterChipsShouldBe(value: number): Chainable<Element>
  }
}
