/* eslint-disable no-unused-expressions */
/* eslint-disable no-undef */

// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
// Cypress.Commands.add('login', (email, password) => { ... })
//
//
// -- This is a child command --
// Cypress.Commands.add('drag', { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add('dismiss', { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This will overwrite an existing command --
// Cypress.Commands.overwrite('visit', (originalFn, url, options) => { ... })

/**
     * Cypress does not currently have a good way to handle waiting for re-render, which
     * is a core implementaion of React. Enforcing an ambguous wait time is
     * a poor but simple workaround for now.
     * https://github.com/cypress-io/cypress/issues/7306#issuecomment-1098106831
     * 
     */
Cypress.Commands.overwrite('click', (originalFn, subject, ...args) => {
  originalFn(subject, ...args)
  cy.wait(1000)
})

Cypress.Commands.add('makeSelectionFromEachDropdown', () => {
  cy.get('#projects-dropdown').click()
  cy.get('#projects-dropdown-listbox').children().first().click()
  cy.get('#categories-dropdown').click()
  cy.get('#categories-dropdown-listbox').children().first().click()
  cy.get('#status-dropdown').click()
  cy.get('#status-dropdown-listbox').children().first().click()
  cy.get('#openedby-dropdown').click()
  cy.get('#openedby-dropdown-listbox').children().first().click()
  cy.numOfFilterChipsShouldBe(4)
})

Cypress.Commands.add('numOfFilterChipsShouldBe', (value) => {
  expect(cy.get('.MuiChip-label').should('have.length', value))
})
