// the backend utilized for development API interaction is referenced in .env.development.local
// NOTE - if testing active development code, ensure the app is running before executing tests
describe('The app should', () => {
  // You can add a test environment url here - ex: https://crdemo.dev.radiologics.com/app/template/QueryTrackerDashBoard.vm
  // note - You need to be logged in first (via seperate tab)
  const page = '' || 'http://127.0.0.1:3000/query-dashboard-ui'
  beforeEach(() => {
    cy.visit(page)
  })

  it('load the UI without crashing', () => {
    expect(cy.contains('Query Dashboard').should('exist'))
    expect(cy.contains('Query List').should('exist'))
    expect(cy.contains('Statistics').should('exist'))
    expect(cy.contains('Export').should('exist'))
    expect(cy.contains('Projects').should('exist'))
    expect(cy.contains('Categories').should('exist'))
    expect(cy.contains('Status').should('exist'))
    expect(cy.contains('Opened By').should('exist'))
    expect(cy.get('.MuiDataGrid-columnHeaderTitleContainer').should('have.length', 7))
  })

  it('switch to the Statistics tab', () => {
    cy.contains('Statistics').click()
    expect(cy.get('.MuiPaper-root > .MuiCardHeader-root').should('have.length', 5))
  })

  it('select a Project option and have the Project options and Chips updated', () => {
    cy.get('#projects-dropdown').click()
    cy.get('#projects-dropdown-listbox').children().first().click()
    expect(cy.get('#projects-dropdown-listbox').children().should('have.length', 1))
    cy.numOfFilterChipsShouldBe(1)
  })
  it('select a Category option and have the Category options and Chips updated', () => {
    cy.get('#categories-dropdown').click()
    cy.get('#categories-dropdown-listbox').children().first().click()
    expect(cy.get('#categories-dropdown-listbox').children().should('have.length', 1))
    cy.numOfFilterChipsShouldBe(1)
  })

  it('select a Status option and have the Status options and Chips updated', () => {
    cy.get('#status-dropdown').click()
    cy.get('#status-dropdown-listbox').children().first().click()
    cy.numOfFilterChipsShouldBe(1)
  })

  it('select a Opened By option and have the Opened By options and Chips updated', () => {
    cy.get('#openedby-dropdown').click()
    cy.get('#openedby-dropdown-listbox').children().first().click()
    cy.numOfFilterChipsShouldBe(1)
  })

  it('allow selections from every dropdown', () => {
    cy.makeSelectionFromEachDropdown()
  })

  it('allow an option to be selected via dropdown, and then unselected', () => {
    cy.get('#projects-dropdown').click()
    cy.get('#projects-dropdown-listbox').children().first().click()
    cy.numOfFilterChipsShouldBe(1)
    cy.get('#projects-dropdown-listbox').children().first().click()
    cy.numOfFilterChipsShouldBe(0)
  })

  it('allow an option to be selected via dropdown, and then unselected via clicking on its chip', () => {
    cy.get('#projects-dropdown').click()
    cy.get('#projects-dropdown-listbox').children().first().click()
    cy.numOfFilterChipsShouldBe(1)
    cy.get('[data-testid="CancelIcon"]').click()
    cy.numOfFilterChipsShouldBe(0)
  })

  it('allow selections from every dropdown while on the Statistics tab', () => {
    cy.contains('Statistics').click()
    expect(cy.get('.MuiPaper-root > .MuiCardHeader-root').should('have.length', 5))
    cy.makeSelectionFromEachDropdown()
  })

  it('allow multiple filter chips to be deselected while on the Statistics tab', () => {
    cy.contains('Statistics').click()
    expect(cy.get('.MuiPaper-root > .MuiCardHeader-root').should('have.length', 5))
    cy.makeSelectionFromEachDropdown()
    cy.get('[data-testid="CancelIcon"]').each((child) => {
      cy.wrap(child).click()
    })
    cy.numOfFilterChipsShouldBe(0)
  })
})
